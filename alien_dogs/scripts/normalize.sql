DROP TABLE IF EXISTS dog_breeds CASCADE;
DROP TABLE IF EXISTS owners CASCADE;


SELECT DISTINCT(dog_breed)
    AS description
  INTO dog_breeds
  FROM import_data.alien_data;

 ALTER TABLE dog_breeds
   ADD id SERIAL PRIMARY KEY; -- perhaps make breed_id later

SELECT DISTINCT(name)
    AS description
  INTO owners
  FROM import_data.alien_data;

 ALTER TABLE owners
   ADD id SERIAL PRIMARY KEY; -- perhaps make breed_id later

  DROP TABLE IF EXISTS dogs;
CREATE TABLE dogs(
	id 		SERIAL PRIMARY KEY,
	name		TEXT,
	owner_id	INT REFERENCES owners(id),
	dog_breed_id 	INT REFERENCES dog_breeds(id)
    );

INSERT INTO dogs(
	name,
	owner_id,
	dog_breed_id
     )

SELECT
	import_data.alien_data.dog_name,
	owners.id 	AS owner_id,
	dog_breeds.id 	AS dog_breed_id
  FROM import_data.alien_data
  LEFT JOIN dog_breeds
    on dog_breeds.description
     = import_data.alien_data.dog_breed

  LEFT JOIN owners 
    on owners.description
     = import_data.alien_data.name;
