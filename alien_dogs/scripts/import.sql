CREATE SCHEMA IF NOT EXISTS import_data;
  DROP TABLE IF EXISTS import_data.alien_data;
 
CREATE TABLE import_data.alien_data(
	name		TEXT,
	dog_breed	TEXT,
	dog_name	TEXT,
	age		TEXT
    );

