CREATE SCHEMA IF NOT EXISTS import;
  DROP TABLE IF EXISTS import.master_plan;
 
CREATE TABLE import.master_plan(
	start_time_utc		TEXT,
	duration		TEXT,
	date			TEXT,
	team			TEXT,
	spass_type		TEXT,
	target			TEXT,
	request_name		TEXT,
	library_definition	TEXT,
	title			TEXT,
	description		TEXT
    );	
	
COPY import.master_plan FROM '/home/master_plan.csv' WITH DELIMITER ',' HEADER CSV;
DROP TABLE IF EXISTS events CASCADE;
DROP TABLE IF EXISTS teams CASCADE;
DROP TABLE IF EXISTS targets CASCADE;
DROP TABLE IF EXISTS spass_types CASCADE;
DROP TABLE IF EXISTS requests CASCADE;
DROP TABLE IF EXISTS event_types CASCADE;

SELECT DISTINCT (team)
AS description
INTO teams
FROM import.master_plan;

ALTER TABLE teams
ADD id SERIAL PRIMARY KEY;

SELECT DISTINCT (library_definition)
AS description
INTO event_types
FROM import.master_plan;

ALTER TABLE event_types
ADD id SERIAL PRIMARY KEY;

SELECT DISTINCT (target)
AS description
INTO targets
FROM import.master_plan;

ALTER TABLE targets
ADD id SERIAL PRIMARY KEY;

SELECT DISTINCT (spass_type)
AS description
INTO spass_types
FROM import.master_plan;

ALTER TABLE spass_types
ADD id SERIAL PRIMARY KEY;

SELECT DISTINCT (request_name)
AS description
INTO requests
FROM import.master_plan;

ALTER TABLE requests
ADD id SERIAL PRIMARY KEY;


DROP TABLE IF EXISTS events;
CREATE TABLE events(
	id 		SERIAL PRIMARY KEY,
	time_stamp 	TIMESTAMPTZ NOT NULL,
	title 		VARCHAR(500),
	description 	TEXT,
	event_type_id 	INT REFERENCES event_types(id),
	target_id	INT REFERENCES targets(id),
	team_id		INT REFERENCES teams(id),
	request_id	INT REFERENCES requests(id),
	spass_type_id	INT REFERENCES spass_types(id)
);


insert into events(
	  time_stamp, 
	  title, 
	  description, 
	  event_type_id, 
	  target_id, 
	  team_id, 
	  request_id,
	  spass_type_id
)	
select 
  import.master_plan.start_time_utc::timestamp, 
  import.master_plan.title, 
  import.master_plan.description,
  event_types.id as event_type_id,
  targets.id as target_id,
  teams.id as team_id,
  requests.id as request_id,
  spass_types.id as spass_type_id
from import.master_plan
left join event_types 
  on event_types.description 
  = import.master_plan.library_definition
left join targets 
  on targets.description 
  = import.master_plan.target
left join teams 
  on teams.description 
  = import.master_plan.team
left join requests 
  on requests.description 
  = import.master_plan.request_name
left join spass_types 
  on spass_types.description 
  = import.master_plan.spass_type;

